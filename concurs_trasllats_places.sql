CREATE TABLE IF NOT EXISTS `ct_places_19` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `st` int NOT NULL,
  `pri_sec` CHAR NOT NULL,
  `codi_centre` VARCHAR(8) NOT NULL,
  `nom_centre` varchar(255) NOT NULL,
  `codi_especialitat` varchar(5) NOT NULL,
  `nom_especialitat` varchar(255) NOT NULL,
  `places` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
)