1/9                                     Diari Oficial de la Generalitat de Catalunya               Núm. 8384 - 12.4.2021


                                                                                            CVE-DOGC-A-21098046-2021




 CÀRRECS I PERSONAL

DEPARTAMENT D'EDUCACIÓ

RESOLUCIÓ EDU/983/2021, d'1 d'abril, per la qual es fa pública la relació de vacants definitives
corresponents al concurs de trasllats adreçat al personal funcionari de carrera i en pràctiques per proveir
llocs de treball vacants a la Inspecció d'Educació i llocs de treball vacants corresponents als cossos
d'ensenyaments secundaris i de règim especial als instituts i instituts escola, a les escoles oficials
d'idiomes i a les escoles d'art i superiors de disseny.



La Resolució EDU/250/2021, de 2 de febrer, va fer públiques les vacants provisionals corresponents al concurs
de trasllats adreçat al personal funcionari de carrera i en pràctiques per proveir llocs de treball vacants en la
Inspecció d'Educació i llocs de treball vacants corresponents als cossos d'ensenyaments secundaris i de règim
especial als instituts i instituts escola, a les escoles oficials d'idiomes i a les escoles d'art i superiors de disseny,
convocat per la Resolució EDU/2669/2020, de 26 d'octubre i es va obrir un termini de 10 dies hàbils per
presentar reclamacions.

D'acord amb el que disposa la base 10 de l'annex 1 de la Resolució EDU/2669/2020, de 26 d'octubre, amb
anterioritat a la resolució definitiva del concurs esmentat, la Direcció General de Professorat i Personal de
Centres Públics ha de fer pública al DOGC la relació de les vacants definitives objecte de provisió.

Per això, vistes les reclamacions presentades durant el termini establert i la revisió efectuada d'acord amb les
noves dades de planificació educativa, tal i com preveu la base 10 de la convocatòria,



Resolc:



-1 Elevar a definitiva la relació de les vacants provisionals de llocs de treball de la Inspecció d'Educació, dels
cossos d'ensenyaments secundaris i de règim especial als instituts i instituts escola, a les escoles oficials
d'idiomes i a les escoles d'art i superiors de disseny, fetes públiques els annexos 1 i 2 de la Resolució
EDU/250/2021, de 2 de febrer, amb les modificacions que es detallen a l'annex, en els termes següents:

Apartat a: llocs de treball que no apareixien a la Resolució de vacants provisionals i dels quals ara hi ha alguna
vacant.

Apartat b: llocs de treball que constaven a la Resolució de vacants provisionals i que han deixat de tenir
vacants.

Apartat c: llocs de treball en què es produeix una variació en relació amb el nombre de vacants publicat en la
Resolució de vacants provisionals.

Es publiquen els centres ordenats per Serveis Territorials, mantenint els mateixos criteris que en els annexos
de la Resolució EDU/2669/2020, de 26 d'octubre, de convocatòria de concurs de trasllats.



-2 Amb la publicació d'aquesta Resolució es consideren contestades, a tots els efectes, les reclamacions
presentades contra la Resolució EDU/250/2021, de 2 de febrer.



Contra aquesta Resolució, que exhaureix la via administrativa, les persones interessades poden interposar
recurs contenciós administratiu davant el Jutjat Contenciós Administratiu, en el termini de dos mesos a
comptar des de l'endemà de la seva publicació al DOGC, de conformitat amb el que preveu l'article 46.1 de la
Llei 29/1998, de 13 de juliol, reguladora de la jurisdicció contenciosa administrativa.

Així mateix, poden interposar potestativament recurs de reposició, previ al recurs contenciós administratiu,




ISSN 1988-298X                                     https://www.gencat.cat/dogc                              DL B 38014-2007
2/9                                    Diari Oficial de la Generalitat de Catalunya         Núm. 8384 - 12.4.2021


                                                                                      CVE-DOGC-A-21098046-2021

davant el director general de Professorat i Personal de Centres Públics, en el termini d'un mes a comptar de
l'endemà de la seva publicació al DOGC, segons el que disposen l'article 77 de la Llei 26/2010, del 3 d'agost,
de règim jurídic i de procediment de les administracions públiques de Catalunya, i els articles 123 i 124 de la
Llei 39/2015, d'1 d'octubre, del procediment administratiu comú de les administracions públiques, o qualsevol
altre recurs que considerin convenient per a la defensa dels seus interessos.




Barcelona, 1 d'abril de 2021




José Ignacio García Plata

Director general de Professorat i Personal de Centres Públics




Annex



Modificacions de les vacants provisionals en centres d'ensenyaments secundaris publicades en l'annex 1 de la
Resolució EDU/250/2021, de 2 de febrer, i que s'eleven a definitives.



(1) Centre sense barreres arquitectòniques (amb rampes i ascensor o planta baixa)

(2) Centre que només disposa de rampes

(3) Centre que només disposa d'ascensor




a) Llocs de treball amb vacants que s'afegeixen als publicats per la Resolució EDU/250/2021, de 2 de febrer



Serveis Territorials a Barcelona Comarques



Zona: 080157 Badalona



Municipi: 080150000 Badalona



Centres, especialitats i nombre de vacants:



08046751 Institut Ventura Gassol (3)

051 Llengua catalana i literatura 01

016 Música 01



Zona: 081017 L'Hospitalet de Llobregat




ISSN 1988-298X                                    https://www.gencat.cat/dogc                        DL B 38014-2007
3/9                                      Diari Oficial de la Generalitat de Catalunya         Núm. 8384 - 12.4.2021


                                                                                        CVE-DOGC-A-21098046-2021



Municipi: 081010000 L'Hospitalet de Llobregat



Centres, especialitats i nombre de vacants:



08076467 Institut Escola Pere Lliscart (1)

006 Matemàtiques 01



Serveis Territorials al Baix Llobregat



Zona: 080737 Cornellà de Llobregat



Municipi: 080730000 Cornellà de Llobregat



Centres, especialitats i nombre de vacants:



08016781 Institut Esteve Terradas i Illa (1)

011 Anglès 01

105 Formació i orientació laboral 01

005 Geografia i història 01

004 Llengua castellana i literatura 01

051 Llengua catalana i literatura 01



Municipi: 080770000 Esplugues de Llobregat



Centres, especialitats i nombre de vacants:



08017131 Institut Severo Ochoa (1)

220 Procediments sanitaris i assistencials 01



Zona: 081147 Martorell



Municipi: 080690000 Collbató



Centres, especialitats i nombre de vacants:



08063801 Institut de Collbató (1)




ISSN 1988-298X                                      https://www.gencat.cat/dogc                       DL B 38014-2007
4/9                                    Diari Oficial de la Generalitat de Catalunya         Núm. 8384 - 12.4.2021


                                                                                      CVE-DOGC-A-21098046-2021

006 Matemàtiques 01



Zona: 083017 Viladecans



Municipi: 080560000 Castelldefels



Centres, especialitats i nombre de vacants:



08045537 Institut Josep Lluís Sert (1)

011 Anglès 01



Municipi: 083010000 Viladecans



Centres, especialitats i nombre de vacants:



08045860 Institut Miramar (1)

051 Llengua catalana i literatura 02



Serveis Territorials al Vallès Occidental



Zona: 081877 Sabadell



Municipi: 081870000 Sabadell



Centres, especialitats i nombre de vacants:



08024893 Institut Castellarnau (1)

105 Formació i orientació laboral 01



Serveis Territorials al Maresme-Vallès Oriental



Zona: 082027 Sant Celoni



Municipi: 082070000 Sant Esteve de Palautordera



Centres, especialitats i nombre de vacants:




ISSN 1988-298X                                    https://www.gencat.cat/dogc                       DL B 38014-2007
5/9                                      Diari Oficial de la Generalitat de Catalunya         Núm. 8384 - 12.4.2021


                                                                                        CVE-DOGC-A-21098046-2021

08065469 Institut Can Record (1)

011 Anglès 01

004 Llengua castellana i literatura 01

018 Orientació educativa 01



Serveis Territorials a la Catalunya Central



Zona: 080227 Berga



Municipi: 080160000 Bagà



Centres, especialitats i nombre de vacants:



08052891 Institut L'Alt Berguedà (1)

009 Dibuix 01



Serveis Territorials a Girona



Zona: 170087 Santa Coloma de Farners



Municipi: 171800000 Santa Coloma de Farners



Centres, especialitats i nombre de vacants:



17005376 Institut de Santa Coloma de Farners (1)

017 Educació física 01




b) Llocs de treball que deixen de tenir les vacants publicades per la Resolució EDU/250/2021, de 2 de febrer



Consorci d'Educació de Barcelona



Zona: 080197 Barcelona



Municipi: 080190000 Barcelona



Centres, especialitats i nombre de vacants:




ISSN 1988-298X                                      https://www.gencat.cat/dogc                       DL B 38014-2007
6/9                                      Diari Oficial de la Generalitat de Catalunya         Núm. 8384 - 12.4.2021


                                                                                        CVE-DOGC-A-21098046-2021



08044156 EA Deià (3)

004 Llengua castellana i literatura 01

051 Llengua catalana i literatura 01



08052608 Institut Pau Claris (1)

012 Alemany 01



Serveis Territorials a Barcelona Comarques



Zona: 081017 L'Hospitalet de Llobregat



Municipi: 081010000 L'Hospitalet de Llobregat



Centres, especialitats i nombre de vacants:



08076467 Institut Escola Pere Lliscart (1)

008 Biologia i geologia 01



Serveis Territorials al Baix Llobregat



Zona: 083017 Viladecans



Municipi: 080560000 Castelldefels



Centres, especialitats i nombre de vacants:



08047480 Institut Mediterrània (1)

051 Llengua catalana i literatura 01



Municipi: 083010000 Viladecans



Centres, especialitats i nombre de vacants:



08068495 Institut Josefina Castellví i Piulachs (1)

009 Dibuix 01




ISSN 1988-298X                                      https://www.gencat.cat/dogc                       DL B 38014-2007
7/9                                    Diari Oficial de la Generalitat de Catalunya         Núm. 8384 - 12.4.2021


                                                                                      CVE-DOGC-A-21098046-2021

08045860 Institut Miramar (1)

019 Tecnologia 01



Serveis Territorials al Vallès Occidental



Zona: 081847 Rubí



Municipi: 082050000 Sant Cugat del Vallès



Centres, especialitats i nombre de vacants:



08031861 Institut Leonardo da Vinci (1)

016 Música 01



Zona: 081877 Sabadell



Municipi: 080510000 Castellar del Vallès



Centres, especialitats i nombre de vacants:



08076558 Institut Escola Sant Esteve

017 Educació física 01

019 Tecnologia 01



Serveis Territorials al Maresme-Vallès Oriental



Zona: 081217 Mataró



Municipi: 081210000 Mataró



Centres, especialitats i nombre de vacants:



08052979 Institut Josep Puig i Cadafalch (3)

019 Tecnologia 01



Serveis Territorials a Girona




ISSN 1988-298X                                    https://www.gencat.cat/dogc                       DL B 38014-2007
8/9                                  Diari Oficial de la Generalitat de Catalunya         Núm. 8384 - 12.4.2021


                                                                                    CVE-DOGC-A-21098046-2021

Zona: 170237 Blanes



Municipi: 170230000 Blanes



Centres, especialitats i nombre de vacants:



17005731 Institut S'Agulla (2)

017 Educació física 01



Zona: 171147 Olot



Municipi: 71140000 Olot



Centres, especialitats i nombre de vacants:



17007981 EOI d'Olot (1)

011 Anglès 01



Serveis Territorials a Tarragona



Zona: 430387 Cambrils



Municipi: 439050000 Salou



Centres, especialitats i nombre de vacants:



43007543 Institut Jaume I (1)

225 Serveis a la comunitat 01



Zona: 431237 Reus



Municipi: 430920000 Mont-roig del Camp



Centres, especialitats i nombre de vacants:



43012988 Institut de Miami

016 Música 01




ISSN 1988-298X                                  https://www.gencat.cat/dogc                       DL B 38014-2007
9/9                                  Diari Oficial de la Generalitat de Catalunya          Núm. 8384 - 12.4.2021


                                                                                    CVE-DOGC-A-21098046-2021




c) No hi ha llocs de treball dels quals es modifiquen les vacants publicades per la Resolució EDU/250/2021, de
2 de febrer.



(21.098.046)




ISSN 1988-298X                                  https://www.gencat.cat/dogc                        DL B 38014-2007
