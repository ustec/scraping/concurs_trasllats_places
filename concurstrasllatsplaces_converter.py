#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# (c) 2017 Mònica Ramírez Arceda <monica@probeta.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import logging
import os
import re
import requests
import shutil
import tempfile
from sqlalchemy.orm import mapper
from sh import pdftotext

from config import ConfigConcursTrasllatsPlaces
from data_converter.data_converter import DataConverter
from concurs_trasllats_places.concurstrasllatsplaces import ConcursTrasllatsPlaces

class ConcursTrasllatsPlacesConverter(DataConverter):
    def __init__(self, config_file_path):
        self.config = ConfigConcursTrasllatsPlaces(config_file_path)
        super(ConcursTrasllatsPlacesConverter, self).__init__()
        mapper(ConcursTrasllatsPlaces, self.table) 
        logging.basicConfig(filename=self.config.log_file, 
                            level=logging.WARNING)

    def get_input_files(self):
        # Loop all URLS downloading PDFs
        urls = open(self.config.urls).readlines()
        for l in urls:
            # filename is the last part of the URL
            filename = l.strip().split("/")[-1]
            # Download the file and save it
            response = requests.get(l)
            with open(os.path.join(self.config.raw_data_dir, filename), 'wb') as f:
                f.write(response.content)
    
    def convert(self):
        for filename in os.listdir(self.config.raw_data_dir):
            if filename == '1845187.pdf':
                filenametxt = 'secundaria.txt'
            else:
                filenametxt = 'primaria.txt'
            pdftotext('-layout',
                  os.path.join(self.config.raw_data_dir, filename),
                  os.path.join(self.config.data_dir, filenametxt))
    
    def get_valid_records(self):
        # Crear una llista de registres amb camps (poden contenir més dades que les que ens interessen)
        pass
    
    def build_objects(self, raw_records):
        # Crear una llista d'objectes de tipus Xxx amb les dades que ens interessen
        pass